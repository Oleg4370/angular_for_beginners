import {Component, OnInit} from '@angular/core';
import { FireBaseApiService } from '../../firebase-api.service';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  title = 'Angular';
  toBuyList: Item[] = [];

  constructor(private fireBaseApiService: FireBaseApiService) {
    this.fireBaseApiService.initDB();
  }

  ngOnInit() {
    this.fireBaseApiService.dataStream.subscribe((newList) => {
      this.toBuyList = newList;
    });
  }

  add() {
    const testItem = {
      title: 'newItem -' + (+(new Date())),
      amount: Math.round(Math.random() * 100),
      checked: false
    };

    this.fireBaseApiService.add(testItem);
  }

  update(item) {
    item.title = 'updatedItem -' + (+(new Date()));

    this.fireBaseApiService.update(item.id, item);
  }

  remove(item) {
    this.fireBaseApiService.remove(item.id);
  }

}

