import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Item } from '../../models/Item';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {
  @Input() toBuyList: Item[];
  @Output() add = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() remove = new EventEmitter();

  constructor() { }

  get allChecked() {
    return this.toBuyList.filter((item) => item.checked);
  }

  get allUnchecked() {
    return this.toBuyList.filter((item) => !item.checked);
  }

  addItem() {
    this.add.emit();
  }

  handleChange(item) {
    console.log('item.checked', item.checked);
    item.checked = !item.checked;
    this.update.emit(item);
  }

  updateItem(item: Item) {
    this.update.emit(item);
  }

  removeItem(item: Item) {
    this.remove.emit(item);
  }

  ngOnInit() {
  }

}
