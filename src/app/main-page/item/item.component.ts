import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() item: Item;
  @Output() updateCurrentItem: EventEmitter<Item> = new EventEmitter();
  @Output() removeCurrentItem: EventEmitter<Item> = new EventEmitter();
  @Output() handleChange: EventEmitter<Item> = new EventEmitter();

  update(item: Item) {
    this.updateCurrentItem.emit(item);
  }

  remove(item: Item) {
    this.removeCurrentItem.emit(item);
  }
  toggle(item: Item) {
    this.handleChange.emit(item);
  }

  constructor() { }

  ngOnInit() {
  }

}
