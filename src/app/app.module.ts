import { BrowserModule } from '@angular/platform-browser';
import { MainPageModule } from './main-page/main-page.module';
import { NgModule } from '@angular/core';


import { MainComponent } from './main-page/main/main.component';
import { FireBaseApiService } from './firebase-api.service';


@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    MainPageModule
  ],
  providers: [FireBaseApiService],
  bootstrap: [MainComponent]
})
export class AppModule { }
